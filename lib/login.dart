import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TelaDeLogin extends StatelessWidget {
  static const String routeName = "/login_screen";
  const TelaDeLogin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text("Tela de login"),
        ),
        body: const Placeholder());
  }
}
