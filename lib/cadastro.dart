import 'package:flutter/material.dart';

class TelaDeCadastro extends StatefulWidget {
  static const String routeName = "/cadastro";
  const TelaDeCadastro({super.key});

  @override
  State<TelaDeCadastro> createState() => _TelaDeCadastroState();
}

class _TelaDeCadastroState extends State<TelaDeCadastro> {
  TextEditingController textEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text("Tela de cadastro"),
        ),
        body: Container(
            child: Column(
          children: [
            TextField(
              controller: textEditingController,
              onChanged: (value) => print(value),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  onPressed: () => Navigator.of(context)
                      .pushNamed(Navigator.defaultRouteName),
                  child: Text("Voltar")),
            )
          ],
        )));
  }
}
